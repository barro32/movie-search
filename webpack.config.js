module.exports = {
	entry: './src/main.js',
	output: {
		filename: 'build/app.js'
	},
	module: {
		rules: [
			{	test: /\.js$/, exclude: /(node_modules)/,	loader: 'babel-loader' }
		]
	}
}
